import { Meteor } from 'meteor/meteor';
import { Match } from 'meteor/check';
import restler from 'restler';
import crypto from 'crypto';
import s from 'underscore.string';

import { estimatesQuerySchema } from '../lib/estimates-query.schema';

Meteor.publish('estimates', function (query) {
    const sub = this;

    if (!Match.test(query, estimatesQuerySchema)) {
        sub.ready();
        return;
    }

    restler
        .get('https://api.uber.com/v1/estimates/price', {
            query,
            headers: {
                'Authorization': `Token ${Meteor.settings.uber.serverToken}`,
            }
        })
        .on('complete', (data, res) => {
            if (res.statusCode !== 200) {
                sub.error(new Meteor.Error(res.statusCode, data.message));
                return;
            }

            data.prices.forEach(p => {
                const estimate = {};

                estimate.service = `After${s(p.display_name).capitalize().value()}`;

                if (p.low_estimate) {
                    estimate.low = Math.round(p.low_estimate * 80) / 100;
                }

                if (p.high_estimate) {
                    estimate.high = Math.round(p.high_estimate * 80) / 100;
                }

                const hash = crypto.createHash('sha256');
                hash.update(p.product_id);
                const _id = new Mongo.ObjectID(hash.digest('hex').slice(0, 24));

                sub.added('estimates', _id, estimate);
            });

            sub.ready();
        });
});

Meteor.publish('addresses', function (address) {
    const sub = this;

    if (!Match.test(address, String) || !address.length) {
        sub.ready();
        return;
    }

    restler
        .get('https://maps.googleapis.com/maps/api/geocode/json', {
            query: {
                address,
                key: Meteor.settings.google.apiKey,
            }
        })
        .on('complete', data => {
            data.results.forEach(r => {
                const hash = crypto.createHash('sha256');
                hash.update(r.place_id);
                const _id = new Mongo.ObjectID(hash.digest('hex').slice(0, 24));
                r._address = address;
                sub.added('addresses', _id, r);
            });
            sub.ready();
        });
});
