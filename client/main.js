import { Template } from 'meteor/templating';
import { Mongo } from 'meteor/mongo';
import { Session } from 'meteor/session';
import debounce from 'debounce';

import './main.html';

const addressesCollection = new Mongo.Collection('addresses', { idGeneration: 'MONGO' });
const estimatesCollection = new Mongo.Collection('estimates', { idGeneration: 'MONGO' });

window.addressesCollection = addressesCollection;
window.estimatesCollection = estimatesCollection;

Template.hello.onCreated(function helloOnCreated() {
    this.autorun(() => {
        this.subscribe('estimates', Session.get('query'));
    });

    this.autorun(() => {
        this.subscribe('addresses', Session.get('from'))
    });

    this.autorun(() => {
        this.subscribe('addresses', Session.get('to'))
    });
});

Template.hello.helpers({
    estimates: () => estimatesCollection.find(),
    fromValue: () => Session.get('from'),
    toValue: () => Session.get('to'),
    fromVariants: () => addressesCollection.find({ _address: Session.get('from') }),
    toVariants: () => addressesCollection.find({ _address: Session.get('to') }),
});

Template.hello.events({
    'keyup input[name="from"]': debounce((e) => {
        Session.set('from', e.target.value);
    }, 300),
    'keyup input[name="to"]': debounce((e) => {
        Session.set('to', e.target.value);
    }, 300),
    'click .setFromBtn'(e) {
        e.preventDefault();
        Session.set('from', this.formatted_address);
    },
    'click .setToBtn'(e) {
        e.preventDefault();
        Session.set('to', this.formatted_address);
    },
    'submit form'(e) {
        e.preventDefault();
        const from = addressesCollection.findOne({ _address: Session.get('from') });
        const to = addressesCollection.findOne({ _address: Session.get('to') });

        if (from && to) {
            Session.set('query', {
                start_latitude: from.geometry.location.lat,
                start_longitude: from.geometry.location.lng,
                end_latitude: to.geometry.location.lat,
                end_longitude: to.geometry.location.lng,
            });
        }
    }
});
