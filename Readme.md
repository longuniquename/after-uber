AfterUber
=========

To start project, you should update `setting.json` with your credentials
and run:

```
curl https://install.meteor.com/ | sh
meteor npm i
meteor npm start
```
