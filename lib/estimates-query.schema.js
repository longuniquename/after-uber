import { SimpleSchema } from 'meteor/aldeed:simple-schema';

export const estimatesQuerySchema = new SimpleSchema({
    start_latitude: {
        type: Number,
        decimal: true,
    },
    start_longitude: {
        type: Number,
        decimal: true,
    },
    end_latitude: {
        type: Number,
        decimal: true,
    },
    end_longitude: {
        type: Number,
        decimal: true,
    },
    seat_count: {
        type: Number,
        optional: true,
    }
});
